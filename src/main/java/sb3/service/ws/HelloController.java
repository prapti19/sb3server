package sb3.service.ws;

import org.springframework.web.bind.annotation.RestController;

import sb3.modifiedParser.Sb3Json2AST;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
    
    @CrossOrigin
    @PostMapping("/analyze")
    public String analyze(@RequestBody String jsonstr) {
    	//String jsonstr = "{\"targets\":[{\"isStage\":true,\"name\":\"Stage\",\"variables\":{\"`jEk@4|i[#Fk?(8x)AV.-my variable\":[\"my variable\",0]},\"lists\":{},\"broadcasts\":{},\"blocks\":{},\"currentCostume\":0,\"costumes\":[{\"assetId\":\"cd21514d0531fdffb22204e0ec5ed84a\",\"name\":\"backdrop1\",\"md5ext\":\"cd21514d0531fdffb22204e0ec5ed84a.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":1,\"rotationCenterY\":1}],\"sounds\":[{\"assetId\":\"83a9787d4cb6f3b7632b4ddfebf74367\",\"name\":\"pop\",\"dataFormat\":\"wav\",\"format\":\"\",\"rate\":44100,\"sampleCount\":1032,\"md5ext\":\"83a9787d4cb6f3b7632b4ddfebf74367.wav\"}],\"volume\":100,\"tempo\":60,\"videoTransparency\":50,\"videoState\":\"off\"},{\"isStage\":false,\"name\":\"Sprite1\",\"variables\":{},\"lists\":{},\"broadcasts\":{},\"blocks\":{\"c_j)IqbYcWU2-RPw(JUl\":{\"opcode\":\"data_setvariableto\",\"next\":\"mTM)V[oFoQ!}D{xcErsA\",\"parent\":null,\"inputs\":{\"VALUE\":[3,\"4I[H`XYn~ZCf)^C)_P~D\",[10,\"0\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":true,\"shadow\":false,\"x\":234,\"y\":-205},\"mTM)V[oFoQ!}D{xcErsA\":{\"opcode\":\"data_changevariableby\",\"next\":\"YxuH#S2wIXL[kcS,Z()I\",\"parent\":\"c_j)IqbYcWU2-RPw(JUl\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":false,\"shadow\":false},\"YxuH#S2wIXL[kcS,Z()I\":{\"opcode\":\"data_changevariableby\",\"next\":\"q|C*,~I*DtNq}}~-5js8\",\"parent\":\"mTM)V[oFoQ!}D{xcErsA\",\"inputs\":{\"VALUE\":[1,[4,\"2\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":false,\"shadow\":false},\"4I[H`XYn~ZCf)^C)_P~D\":{\"opcode\":\"operator_round\",\"next\":null,\"parent\":\"c_j)IqbYcWU2-RPw(JUl\",\"inputs\":{\"NUM\":[3,\"wG]LU63L._fLgGKPp;xR\",[4,\"\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false},\"wG]LU63L._fLgGKPp;xR\":{\"opcode\":\"operator_random\",\"next\":null,\"parent\":\"4I[H`XYn~ZCf)^C)_P~D\",\"inputs\":{\"FROM\":[1,[4,\"1\"]],\"TO\":[1,[4,\"10\"]]},\"fields\":{},\"topLevel\":false,\"shadow\":false},\"q|C*,~I*DtNq}}~-5js8\":{\"opcode\":\"data_changevariableby\",\"next\":null,\"parent\":\"YxuH#S2wIXL[kcS,Z()I\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":false,\"shadow\":false},\"M.hzNlES9?P$df{C#]0f\":{\"opcode\":\"data_setvariableto\",\"next\":\"WNAeZfHG;em)jG#;=v(P\",\"parent\":null,\"inputs\":{\"VALUE\":[1,[10,\"0\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":true,\"shadow\":false,\"x\":1107,\"y\":-593},\"WNAeZfHG;em)jG#;=v(P\":{\"opcode\":\"data_changevariableby\",\"next\":null,\"parent\":\"M.hzNlES9?P$df{C#]0f\",\"inputs\":{\"VALUE\":[1,[4,\"1\"]]},\"fields\":{\"VARIABLE\":[\"my variable\",\"`jEk@4|i[#Fk?(8x)AV.-my variable\"]},\"topLevel\":false,\"shadow\":false}},\"currentCostume\":0,\"costumes\":[{\"assetId\":\"09dc888b0b7df19f70d81588ae73420e\",\"name\":\"costume1\",\"bitmapResolution\":1,\"md5ext\":\"09dc888b0b7df19f70d81588ae73420e.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":47,\"rotationCenterY\":55},{\"assetId\":\"3696356a03a8d938318876a593572843\",\"name\":\"costume2\",\"bitmapResolution\":1,\"md5ext\":\"3696356a03a8d938318876a593572843.svg\",\"dataFormat\":\"svg\",\"rotationCenterX\":47,\"rotationCenterY\":55}],\"sounds\":[{\"assetId\":\"83c36d806dc92327b9e7049a565c6bff\",\"name\":\"Meow\",\"dataFormat\":\"wav\",\"format\":\"\",\"rate\":44100,\"sampleCount\":37376,\"md5ext\":\"83c36d806dc92327b9e7049a565c6bff.wav\"}],\"volume\":100,\"visible\":true,\"x\":0,\"y\":0,\"size\":100,\"direction\":90,\"draggable\":false,\"rotationStyle\":\"all around\"}],\"meta\":{\"semver\":\"3.0.0\",\"vm\":\"0.1.0-prerelease.1528399883\",\"agent\":\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36\"}}";
    	String msg = new Sb3Json2AST().analyze(jsonstr);
    	
    	return msg;
    }

}